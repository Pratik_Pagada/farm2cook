/**
 * Created by InspireUI on 19/02/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import { View, StatusBar } from "react-native";
import { Config, Device, Styles } from "@common";
import {
  MyToastContainer,
  MyNetInfoContainer,
  // AppIntroContainer,
  LeftMenuContainer,
} from "@containers";
import Navigation from "@navigation";
import { connect } from "react-redux";
import { initialApp } from "@redux/operations";
import { toast, closeDrawer } from "./Omni";

// const mapStateToProps = ({ app }) => ({
//   introStatus: app.finishIntro,
// });

@connect(
  null,
  { initialApp }
)
export default class Router extends PureComponent {
  componentDidMount() {
    this.props.initialApp();
  }

  goToScreen = (routeName, params) => {
    if (!this.navigator) {
      return toast("Cannot navigate");
    }
    this.navigator.dispatch({ type: "Navigation/NAVIGATE", routeName, params });
    closeDrawer();
  };

  render() {
    // if (!this.props.introStatus) {
    //   return <AppIntroContainer />;
    // }

    return (
      <LeftMenuContainer
        goToScreen={this.goToScreen}
        type="scale" // default type sidemenu typeof "overlay", "small", "wide", default "scale"
        routes={
          <View style={Styles.Common.appContainer}>
            <StatusBar
              hidden={Device.isIphoneX ? false : !Config.showStatusBar}
            />
            <Navigation ref={(comp) => (this.navigator = comp)} />
            <MyToastContainer />
            <MyNetInfoContainer />
          </View>
        }
      />
    );
  }
}
