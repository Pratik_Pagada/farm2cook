/** @format */

import React, { Component } from "react";
import {
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  View,
} from "react-native";
import { Color, Styles } from "@common";
import * as Animatable from "react-native-animatable";

class NavBarIcon extends Component {
  componentWillReceiveProps(nextProps) {
    if (
      this.props.number &&
      this.refs.menu &&
      this.props.number !== nextProps.number
    ) {
      this.refs.menu.fadeInDown(600);
    }
  }

  render() {
    const { onPress, number, icon, color, size, style } = this.props;
    const iconColor = color || "#333";

    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={[icon, style]}>
          <Image
            source={icon}
            style={[
              styles.icon,
              { tintColor: iconColor },
              {
                width: size || Styles.IconSize.ToolBar,
                height: size || Styles.IconSize.ToolBar,
              },
            ]}
            resizeMode="contain"
          />
          {!number ? null : (
            <Animatable.View ref="menu" style={styles.numberWrap}>
              <Text ellipsizeMode="tail" numberOfLines={1} style={styles.number}>{number}</Text>
            </Animatable.View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  iconWrap: {
    flex: 1,
    alignItems: "center",
  },
  numberWrap: {
    ...Styles.Common.ColumnCenter,
    position: "absolute",
    top: 0,
    right: 10,
    height: 18,
    minWidth: 25,
    backgroundColor: Color.primary,
    borderRadius: 9,
  },
  number: {
    color: "white",
    fontSize: 12,
    marginLeft: 3,
    marginRight: 3,
  },
});

NavBarIcon.defaultProps = {
  number: 0,
};

export default NavBarIcon;
