/** @format */

import React from "react";
import { View, Text, Image, Animated,Dimensions, TouchableOpacity, Alert, AsyncStorage, FlatList, TextInput, ScrollView } from 'react-native';
// import {Animated, View,Text,TouchableOpacity } from "react-native";
import { isEmpty } from "lodash";
import { Config, Styles, Color } from "@common";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const location = require("../../images/icons/icon-pin.png");
const window = Dimensions.get('window');


const NavBarLocations = (props) => {

  onPlaceSearch = (data, details) => {
    console.log('DATA', data);
    console.log('DETAILS', details);
  }

  const scrollAnimation =
    props && props.navigation
      ? props.navigation.getParam("animatedHeader")
      : new Animated.Value(1);
  return (
    <View style={[Styles.Common.title, {
      width: '100%', height: 50,
      backgroundColor: 'white',
      justifyContent: 'center', alignItems: 'center', opacity: scrollAnimation, flexDirection: 'row'
    }]} >
      <Image style={{ height: 20, width: 15, justifyContent: 'center' }} source={location} />
      <TouchableOpacity style={{ marginLeft: 10, justifyContent: 'center' }}
        onPress={() => scrollAnimation.navigation.navigate('NLocation')}>
        <Text style={{ color: Color.textColor }}>{"Location"}</Text>
      </TouchableOpacity>
    </View>


  );
  // this.openSearchModal()
};

export default NavBarLocations;
