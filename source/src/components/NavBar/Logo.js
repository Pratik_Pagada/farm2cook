/** @format */

import React from "react";
import { Animated,Text,TouchableOpacity } from "react-native";
import { isEmpty } from "lodash";
import { Config, Styles } from "@common";

const NavBarLogo = (props) => {
  const scrollAnimation =
    props && !isEmpty(props.navigation)
      ? props.navigation.getParam("animatedHeader")
      : new Animated.Value(1);

  return (
    // <Animated.Image
    //   source={Config.LogoImage}
    //   style={[Styles.Common.logo, { opacity: scrollAnimation }]}
    // />
    <TouchableOpacity  onPress={() => {  }}>
    <Text
      style={[Styles.Common.title,{ opacity: scrollAnimation,
      color: '#263237',
      fontSize: 18,}]}>
      {"Farm2Cook"}
    </Text></TouchableOpacity>
  );
};

export default NavBarLogo;
