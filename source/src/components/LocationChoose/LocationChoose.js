import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput,Alert,PermissionsAndroid ,AsyncStorage} from 'react-native';
import styles from './LocationStyle';
import RNGooglePlaces from 'react-native-google-places';
const location = require("../../Images/location.png");

export default class LocationChoose extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount(){
        requestlocationPermission();
    }

    openSearchModal() {
        RNGooglePlaces.openAutocompleteModal()
        .then((place) => {
            console.log(place);
            //Alert.alert("",JSON.stringify(place));
            this.loginCall(place.address.substring(0, place.address.indexOf(',')));
            // place represents user's selection from the
            // suggestions and it is a simplified Google Place object.
        })
        .catch(error => console.log(error.message));  // error is a Javascript Error object
      }

      loginCall (address) {
        AsyncStorage.setItem('userData', address)
        this.props.onLoginPress();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topContainer}>

                </View>



                <View style={styles.bottomContainer}>
                    <View style={styles.bottomLoginContainer}>
                        <Text style={styles.bottomLoginText}>Already have an account? Sign in</Text>
                    </View>
                </View>

                <View style={styles.buttonStart}>
                    <TouchableOpacity >
                        <Text style={[styles.bottomLoginText, { color: 'white' }]}>LET'S START</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.textInputContainer}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Image style={{ height: 20, width: 15, left: 20, marginTop: 15 }} source={location} />
                        {/* <TextInput
                            placeholder="Enter your area"
                            placeholderTextColor="#D3D3D3"
                            underlineColorAndroid="transparent"
                            style={{ flex: 1, marginLeft: 20 }} /> */}

                        <TouchableOpacity
                            style={{ flex: 1, marginLeft: 30,justifyContent:'center' }} 
                            onPress={() => this.openSearchModal()}
                        >
                            <Text style={{color:'white'}}>Pick a Place</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}


async function requestlocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera")
      } else {
        console.log("Camera permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }