import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "yellow"
    },
    topContainer: {
        height: "100%",
        backgroundColor: 'orange'

    },
    bottomContainer: {
        height: "30%",
        backgroundColor: 'white'
    },
    bottomLoginContainer: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0
    },
    bottomLoginText: { textAlign: 'center', color: 'black' },
    buttonStart: {
        width: '90%',
        marginHorizontal:'5%',
        height: 50,
        backgroundColor:'green',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 100,
        borderRadius:6
    },

    textInputContainer: {
        width: '90%',
        marginHorizontal:'5%',
        height: 50,
        backgroundColor:'red',
        justifyContent:'center',
        position: 'absolute',
        bottom: "26.5%",
        borderRadius:6
    },
});
