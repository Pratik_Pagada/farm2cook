/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity, View, Platform, Image, Modal, TouchableHighlight, Dimensions, FlatList } from "react-native";
import { ProductPrice, ImageCache } from "@components";
import { Color, Constants, Languages, Tools } from "@common";
// const plus = require("../../images/plus.png");
const plus = require("../../images/add_128.png");
const out_of_stok = require("../../images/out_of_stok.png");
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import { addToCart } from "@redux/operations";
import { getDefaultProductVariant } from "@redux/selectors";
import { toast } from "@app/Omni";
import QuickPicker from 'quick-picker';

const mapStateToProps = (state, props) => {
  return {
    defaultProductVariant: getDefaultProductVariant(props.product),
    checkoutId: state.carts.checkoutId,
    total: state.carts.total,
  };
};

@withNavigation
@connect(
  mapStateToProps,
  { addToCart }
)
export default class LayoutItem extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      selectedLetter: 1,
    }
  }


  _addToCart = (qty, product) => {
    const { addToCart, checkoutId, total, defaultProductVariant } = this.props;
    if (qty < Constants.LimitAddToCart) {
      addToCart({ item: product, checkoutId, variant: defaultProductVariant, quantity: qty });
    } else {
      alert(Languages.ProductLimitWaring);
    }
  };


  componentDidMount() {
    const myArray = [];
    for (let i = 1; i <= 100; i++) {
      obj = {
        isSelected: 'false',
        value: i
      }
      myArray.push(obj)
    }

    this.setState({
      quentity: myArray
    })
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  renderItem = (item, index) => {
    console.log('ITEM:::::: ', JSON.stringify(item));
    return (
      <View style={{ width: 100, alignItems: 'center' }}>
        <Text style={{ color: 'black' }}>{item.value}</Text>
      </View>
    )
  }

  _onPressText = (product) => () => {
    const { selectedLetter } = this.state;
    if (!product.inStock) {
      toast(Languages.OutOfStock);
    } else {
      QuickPicker.open({
        items: ["Select Quantity", '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50'],
        pickerType: 'normal',
        selectedValue: '1',
        onPressDone: (selectedValueFromPicker) => {
          this.setState({
            selectedLetter: selectedValueFromPicker
          }),
            this._addToCart(selectedValueFromPicker, product);
          toast(selectedValueFromPicker);
          QuickPicker.close()
        }
      });
    }

  }
  _hasWaight = (product)  => {
    return product.weight && product.weight > 0;
  };


  render() {
    const { onPress, title, product, imageURI, size } = this.props;
    return (

      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={onPress}>
          <View style={{ width: size.width }}>
            <ImageCache
              uri={imageURI}
              style={[styles.image, { width: size.width + 10, height: size.height }]}
            />
            <Text numberOfLines={2} style={[styles.text, {  minHeight: 35, width: size.width }]}>
              {title}
            </Text>

            {/* <View style={[styles.rowBottom, { width: size.width }]}> */}
            <ProductPrice product={product}  />
            {/* <Text numberOfLines={1} style={[styles.text, { fontSize: 15, fontWeight: "bold", color: Color.primary, width: size.width }]}>
              {`${Tools.getPrice(product.price)} `}
            </Text> */}
            
            {this._hasWaight(product)?
              <Text numberOfLines={1} style={[styles.text, { fontSize: 12, fontWeight: "normal", color: Color.textColor, width: size.width }]}>
                {product.weight} {product.weightUnit}
              </Text>:<Text></Text>
            }    

              {/* <ProductPrice product={product}  />
          <AddToCartIconContainer product={product} /> */}
            {/* </View> */}
            {/* <WishListIconContainer product={product} /> */}
          </View>
        </TouchableOpacity>
        {/* <View style={[styles.rowBottom, { width: size.width }]}>
          <Text numberOfLines={1} style={[styles.text, { fontSize: 12, fontWeight: "normal", color: Color.textColor, width: size.width }]}>
            {product.weight}{product.weightUnit}
          </Text>
        </View> */}

        <View style={[styles.rowBottom, { width: size.width }]}>
          {product.inStock
            ? <TouchableOpacity activeOpacity={0.9}
              native={false} onPress={this._onPressText(product)}
              style={{ position: 'absolute', right: 0, bottom: 0 }}>
              <Image style={{ height: 28, width: 28, }} source={plus} />
            </TouchableOpacity>
            : <TouchableOpacity activeOpacity={0.9}
              style={{ position: 'absolute', right: 0, bottom: 0 }}>
              <Image style={{ height: 28, width: 28, }} source={out_of_stok} />
            </TouchableOpacity>}
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    alignItems: "flex-start",
    marginBottom: 12,
    borderRadius: 4,
    overflow: "hidden",
    backgroundColor: Color.primary_t,
    marginHorizontal: 5,
  },
  image: {
    overflow: "hidden",
    marginBottom: 1,
  },
  text: {
    fontSize: 13,
    lineHeight: Platform.OS === "android" ? 20 : 16,
    color: Color.TextDefault,
    backgroundColor: Color.primary_t,
    fontWeight: "bold",
    alignSelf: "flex-start",
    marginBottom: 2,
    marginLeft: 4,
  },
  rowBottom: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
};
