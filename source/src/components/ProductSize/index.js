/** @format */

import React from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import { Constants, Color } from "@common";

const ProductSize = (props) => (
  <TouchableOpacity
    onPress={() => props.onPress()}
    style={[
      props.text.length > 2 ? styles.containerLong : styles.container,
      props.style,
      props.selected && styles.active,
    ]}
    activeOpacity={0.8}
    underlayColor="transparent">
    <Text style={[styles.text, props.selected && { color: "white" }]}>
      {props.text}
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  active: {
    backgroundColor: Color.primary,
  },
  container: {
    width: 85,
    height: 45,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f3f4f6",
  },
  containerLong: {
    width: 85,
    height: 45,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f3f4f6",
  },
  text: {
    color:Color.textColor,
    fontSize:14,// 18,
    // fontFamily:Constants.fontFamily,
  },
});

export default ProductSize;
