/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text, StyleSheet } from "react-native";
import { Tools, Constants, Color, Styles } from "@common";

export default class ProductPrice extends PureComponent {
  static propTypes = {
    product: PropTypes.object,
    hideDisCount: PropTypes.bool,
    style: PropTypes.any,
  };

  render() {
    const { product, hideDisCount, style } = this.props;
    return (
      <View style={[styles.priceWrapper, style]}>
        <Text style={[styles.textList, styles.price]}>
          {`${Tools.getPrice(product.price)} `}
        </Text>
        <Text style={[styles.textList, styles.salePrice]}>
          {product.onSale ? Tools.getPrice(product.regularPrice) : ""}
        </Text>
        {/* {hideDisCount && !product.onSale ? (<View />) : (
          <View style={styles.saleWrap}>
            <Text style={[styles.textList, styles.saleOff]}>
              {Tools.getPriceDiscount(product)}
            </Text>
          </View>)} */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  priceWrapper: {
    flexDirection: "row",
  },
  textList: {
    color: Color.black,
    fontSize: Styles.FontSize.medium,
    fontFamily: Constants.fontFamily,
  },
  salePrice: {
    textDecorationLine: "line-through",
    color: Color.blackTextDisable,
    marginLeft: 0,
    marginRight: 0,
    fontWeight: "bold", 
    fontSize: Styles.FontSize.tiny,
  },
  price: {
    color: Color.red,
    fontSize: Styles.FontSize.small,
    fontWeight: "bold", 
    fontFamily: Constants.fontFamily,
  },
  saleWrap: {
    borderRadius: 5,
    backgroundColor: Color.saleWrapprimary,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 3,
    marginLeft: 5,
  },
  saleOff: {
    color: Color.lightTextPrimary,
    fontSize: Styles.FontSize.small,
  },
});
