import React, { Component } from 'react';
import { View, TextInput, ScrollView, AsyncStorage } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class NewLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.onPlaceSearch = this.onPlaceSearch.bind(this);
    }


    onPlaceSearch(data, details) {
        setTimeout(() => {
            AsyncStorage.setItem("chooselocation", data.description.substring(0, data.description.indexOf(',')));
            this.props.navigation.state.params.onGoBack();
            this.props.navigation.goBack();
          }, 50)
        // AsyncStorage.setItem("chooselocation", data.description.substring(0, data.description.indexOf(',')));
        // this.props.navigation.state.params.onGoBack();
        // this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <GooglePlacesAutocomplete
                    enablePoweredByContainer={false}
                    placeholder="Search"
                    minLength={2}
                    autoFocus={false}
                    fetchDetails={true}
                    onPress={(data, details) => this.onPlaceSearch(data, details)}
                    query={{
                        types: '(regions)',
                        key: 'AIzaSyC-u4sNIZWWMn0nayMq08u3uRpq4iFhNww',
                        language: 'en'
                    }}
                    styles={{
                        textInputContainer: {
                            backgroundColor: 'rgba(0,0,0,0)'
                        },
                        listView: {
                            height: '100%',
                            width: '100%',
                            position: 'absolute',
                            top: 40
                        }
                    }}
                    nearbyPlacesAPI={'GooglePlacesSearch'}
                >
                </GooglePlacesAutocomplete>
            </View>
        );
    }
}
