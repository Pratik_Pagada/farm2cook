/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Languages } from "@common";
import styles from "./styles";

export default class ProductListHeader extends PureComponent {
  render() {
    const { title, onPress, hideSeeAll } = this.props;

    if (!title) return null;

    return (
      <View style={styles.header}>
        <View style={styles.headerLeft}>
          <Text style={styles.tagHeader}>{title}</Text>
        </View>
        {!hideSeeAll && (
          <TouchableOpacity onPress={onPress} style={styles.headerRight}>
            <Text style={styles.headerRightText}>
              {Languages.seeAll.toUpperCase()}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
