/** @format */

import React from "react";
import { StyleSheet, TextInput, View } from "react-native";
import { Color, Styles } from "@common";

const Input = (props) => {
  return <StandardTextInput {...props} />;
};

const StandardTextInput = (props) => (
  <View style={styles.container}>
    <TextInput
      style={[Styles.Common.Textinput, styles.textinput, props.inputStyle]}
      placeholderTextColor={Color.TextDefault}
      autoCorrect={false}
      underlineColorAndroid="transparent"
      {...props}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    borderColor: "#cfcfe2",
    borderBottomWidth: 1,
    flex: 1,
    height: 40,
  },
  textinput: {
    textAlign: "left",
    backgroundColor: "transparent",
  },
});

export default Input;
