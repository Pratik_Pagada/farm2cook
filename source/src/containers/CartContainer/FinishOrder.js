/** @format */

import React, { PureComponent } from "react";
import { Text, View, StyleSheet, ScrollView } from "react-native";
import { withNavigation } from "react-navigation";
import Ionicons from "@expo/vector-icons/Ionicons";
import { ShopButton } from "@components";
import { Languages, Color, Constants, Styles } from "@common";

@withNavigation
export default class FinishOrder extends PureComponent {
  _handleNavigate = () => {
    this.props.navigation.navigate("MyOrdersScreen");
  };

  render() {
    return (
      <View style={Styles.Common.CheckoutBoxContainer}>
        <View style={Styles.Common.CheckoutBox}>
          <ScrollView
            contentContainerStyle={Styles.Common.CheckoutBoxScrollView}>
            <View style={styles.iconContainer}>
              <Ionicons
                name="ios-checkmark-circle"
                size={80}
                color={Color.primary}
              />
            </View>

            <Text style={styles.title}>{Languages.ThankYou}</Text>
            <Text style={styles.message}>{Languages.FinishOrder}</Text>
            <ShopButton
              onPress={this._handleNavigate}
              style={styles.button}
              text={Languages.ViewMyOrders}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontSize: 25,
    fontWeight: "bold",
    marginTop: 40,
    fontFamily: Constants.fontFamilyBold,
  },
  iconContainer: {
    alignItems: "center",
    marginTop: 20,
  },
  message: {
    textAlign: "center",
    fontSize: 15,
    color: "gray",
    lineHeight: 25,
    margin: 20,
    fontFamily: Constants.fontFamily,
  },
  button: {
    height: 40,
    width: 160,
    borderRadius: 20,
    backgroundColor: Color.primary,
  },
});
