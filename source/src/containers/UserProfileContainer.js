/** @format */

import React, { PureComponent } from "react";
import { View, ScrollView, Text, Switch, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";
import { toggleNotification, changeCurrency } from "@redux/actions";
import { logoutUserAndCleanCart } from "@redux/operations";
import { toast } from "@app/Omni";

import {
  UserProfileHeader,
  UserProfileRowItem,
  ModalBox,
  CurrencyPicker,
} from "@components";
import { Languages, Color, Tools } from "@common";

const mapStateToProps = ({ user, wishlist, app }) => ({
  wishlistTotal: wishlist.total,
  userInfo: user.userInfo,
  accessToken: user.accessToken,
  language: user.language,
  currency: app.currency,
  enableNotification: app.enableNotification,
});
/**
 * TODO: refactor
 */
@connect(
  mapStateToProps,
  { toggleNotification, changeCurrency, logoutUserAndCleanCart }
)
@withNavigation
export default class UserProfileContainer extends PureComponent {
  /**
   * TODO: refactor to config.js file
   */
  _getListItem = () => {
    const {
      currency,
      wishlistTotal,
      userInfo,
      enableNotification,
    } = this.props;

    const listItem = [
      // {
      //   label: `${Languages.WishList} (${wishlistTotal})`,
      //   routeName: "WishlistScreen",
      // },
      userInfo && {
        label: `${Languages.Address} (${userInfo.addresses.length})`,
        routeName: "UserAddressScreen",
      },
      userInfo && {
        label: Languages.MyOrder,
        routeName: "MyOrdersScreen",
        params: {
          name:"My orders"
        },
      },
      {
        label: Languages.About,
        routeName: "CustomPage",
        params: {
          url: "https://www.farm2cook.com/pages/about-us",
          name:"About US"
        },
      },
    ];

    return listItem;
  };

  _handleSwitch = (value) => {
    this.props.toggleNotification(value);
  };

  _handlePress = (item) => {
    const { navigation } = this.props;
    const { routeName, isActionSheet } = item;

    // toast(routeName);
    // if(routeName == 'MyOrders'){
    //   this.props.navigation.navigate("MyOrdersScreen");
    // }
    if (routeName && !isActionSheet) {
      navigation.navigate(routeName, item.params);
    }

    if (isActionSheet) {
      this.currencyPicker.openModal();
    }
  };

  _handleNavigate = () => {
    this.props.navigation.navigate("MyOrdersScreen");
  };

  _handlePressLogin = () => {
    if (this.props.userInfo) {
      this.props.logoutUserAndCleanCart();
    } else {
      this.props.navigation.navigate("Login");
    }
  };

  render() {
    const { userInfo,accessToken, currency, changeCurrency } = this.props;
    const name = Tools.getName(userInfo);
    const listItem = this._getListItem();

    return (
      <View style={styles.container}>
        <ScrollView>
          <UserProfileHeader
            onPress={this._handlePressLogin}
            user={{
              ...userInfo,
              name,
            }}
          />

          {userInfo && (
            <View style={styles.profileSection}>
              <Text style={styles.headerSection}>
                {Languages.AccountInformations.toUpperCase()}
              </Text>
              <UserProfileRowItem
                label={Languages.Name}
                onPress={this._handlePress}
                value={name}
              />
              <UserProfileRowItem
                label={Languages.Email}
                value={userInfo.email}
              />
              {/* <UserProfileRowItem
                label={Languages.accessToken}
                value={accessToken}
              /> */}
            </View>
          )}

          <View style={styles.profileSection}>
            {listItem.map((item, index) => {
              return (
                item && (
                  <UserProfileRowItem
                    icon
                    key={index.toString()}
                    onPress={() => this._handlePress(item)}
                    {...item}
                  />
                )
              );
            })}
          </View>
        </ScrollView>

        <ModalBox ref={(c) => (this.currencyPicker = c)}>
          <CurrencyPicker currency={currency} changeCurrency={changeCurrency} />
        </ModalBox>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5F5F5",
  },
  profileSection: {
    backgroundColor: "#FFF",
    marginBottom: 15,
  },
  headerSection: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    fontSize: 13,
    color: "#4A4A4A",
    fontWeight: "600",
  },
});
