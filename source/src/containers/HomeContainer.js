/** @format */

import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  FlatList,
  RefreshControl,
  View,
  Text,
  StyleSheet,
  Animated,
  TouchableOpacity,
} from "react-native";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import moment from "moment";
import QuickPicker from 'quick-picker';
import {
  fetchAllProductsLayout,
  fetchAllProducts,
  fetchMoreAllProducts,
  fetchCategories,
} from "@redux/operations";
import { selectCategory } from "@redux/actions";

import { HorizontalList, VerticalList } from "@components";
import { Constants, Color, Styles } from "@common";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const mapStateToProps = ({ category, layout, products }) => ({
  dataLayout: layout.layout,
  layoutFetching: layout.isFetching,
  layoutMode: layout.layoutMode,
  // vertical mode
  category: category,
  selectedCategory: category.selectedCategory,
  list: products.list,
  hasNextPage: products.hasNextPage,
  productFetching: products.isFetching,
  cursor: products.cursor,
});

const mapDispatchToProps = {
  fetchAllProductsLayout,
  fetchAllProducts,
  fetchMoreAllProducts,
  fetchCategories,
  selectCategory
};

@connect(
  mapStateToProps,
  mapDispatchToProps,
  // {   }
)
@withNavigation
export default class HomeContainer extends Component {
  static propTypes = {
    userInfo: PropTypes.object,
    fetchCategories: PropTypes.func.isRequired,
    selectCategory: PropTypes.func.isRequired,
    category: PropTypes.any,
    goToScreen: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      currentDate: moment().format("dddd, DD MMM YYYY"),
    };
    // this.scrollAnimation = new Animated.Value(0);
    this.scrollAnimation = new Animated.Value(1);
  }

  componentWillMount() {
    this.props.navigation.setParams({
      animatedHeader: this.scrollAnimation.interpolate({
        inputRange: [0, 170],
        outputRange: [-1, 1],
        extrapolate: "clamp",
      }),
    });
  }

  componentDidMount() {
    this._fetchAll();
    const { category } = this.props;
    if (category && category.list.length === 0) {
      this.props.fetchCategories();
    }

  }

  componentWillReceiveProps(nextProps) {
    this._handleLoad(nextProps);
  }

  _handleLoad = (newProps) => {
    const { layoutMode } = this.props;
    if (newProps.layoutMode !== layoutMode) {
      // handle load when switch layout vertical list
      if (
        (newProps.list && newProps.list.length === 0) ||
        this._isHorizontal(newProps.layoutMode)
      ) {
        this._fetchAll(newProps.layoutMode);
      }
    }
  };

  _fetchAll = (layoutMode) => {
    if (!this._isHorizontal(layoutMode)) {
      this.props.fetchAllProducts();
    } else {
      this.props.fetchAllProductsLayout();
    }
  };

  _loadMore = () => {
    const { hasNextPage, cursor } = this.props;
    if (hasNextPage && cursor) {
      this.props.fetchMoreAllProducts({ cursor });
    }
  };

  _isHorizontal = (layoutMode) => {
    const mode = layoutMode || this.props.layoutMode;
    return mode === Constants.Layout.horizon;
  };

  _onPressSeeMore = (index, name, item) => {
    this.props.navigation.navigate("ListAllScreen", { index, name });
    // this.props.navigation.navigate("CategoryDetail", { item });
  };


  _renderLoading = () => {
    return this.props.isFetching;
  };

  _renderHeader = () => {
    const { category, selectedCategory } = this.props;
    return (
      <View >
        <View style={{ height: 35, backgroundColor: '#263237', justifyContent: 'center', alignItems: 'center' }}>
          <FlatList
            data={category.list}
            extraData={this.props}
            horizontal={true}
            keyExtractor={this.keyExtractor}
            isActive={
              selectedCategory ? selectedCategory.id === category.list.id : false
            }
            renderItem={this.renderItem} />
        </View>
        <View style={styles.header} >
          {/* <Text style={styles.headerDate}>{this.state.currentDate}</Text>
        <Text style={styles.headerStore}>{Constants.nameStore}</Text> */}
        </View>
      </View>
    );
  };

  _handlePress = ({ item,name }) => {
    const { goToScreen, selectCategory } = this.props;
    const params = {
      ...item,
    };
    selectCategory(params);
    this.props.navigation.navigate("CategoryDetail", { item, name });
    
  };

  keyExtractor = (item, index) => index;

  renderItem = ({ item, index }) => {
    console.log(' ---->Data ==> ', index, (item.length - 1));
    return (
      <TouchableOpacity style={styles.textItem}>
        <Text style={{
          color: 'white', // fontFamily:Constants.fontFamily,
          fontSize: Styles.FontSize.small, 
          textAlign: 'center',
          fontWeight:'bold'
          
        }}
          onPress={() => this._handlePress({ item: item,name:item.title })}> {item.title} {index === (item.length - 1) ? '' : ' | '} </Text>

      </TouchableOpacity>
    );
  }

  _renderHorizontalList = ({ item, index }) => {
    return (
      <View style={styles.section}>
        <HorizontalList
          {...item}
          index={index}
          onPressSeeMore={this._onPressSeeMore}
        />
      </View>
    );
  };

  render() {
    const { dataLayout, layoutFetching } = this.props;
    // // render vertical layout
    // if (!this._isHorizontal()) return this._renderVerticalList();
    const onScroll = Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              y: this.scrollAnimation,
            },
          },
        },
      ],
      { useNativeDriver: true }
    );
    const { category, selectedCategory } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: "#FFF", paddingBottom: 80 }}>
        <View style={{ height: 50, backgroundColor: '#263237', justifyContent: 'center', alignItems: 'center' }}>
          <FlatList
            data={category.list}
            extraData={this.props}
            horizontal={true}
            keyExtractor={this.keyExtractor}
            isActive={
              selectedCategory ? selectedCategory.id === category.list.id : false
            }
            renderItem={this.renderItem} />
        </View>

        <AnimatedFlatList
          data={dataLayout}
          keyExtractor={(item, index) => `h_${index}`}
          renderItem={this._renderHorizontalList}
          scrollEventThrottle={1}
          refreshing={layoutFetching}
          refreshControl={
            <RefreshControl
              refreshing={layoutFetching}
              onRefresh={this._fetchAll}
            />
          }
          {...{ onScroll }}
        />
        
        <QuickPicker  />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
textItem: {
    paddingRight: 5, paddingTop: 5, paddingBottom: 5, height: '100%', justifyContent: 'center',
  },
  section: {
    flex: 1,
    backgroundColor:"#F7F7F7",
    // marginBottom: 5,
  },
  header: {
    paddingVertical: 10,
    marginLeft: 5//Styles.spaceLayout,
  },
});
