/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, Text, Image } from "react-native";
import { Styles, Color, Images } from "@common";
import { connect } from "react-redux";
const SIZE = 80;

const mapStateToProps = ({ carts, wishlist }) => ({ carts, wishlist });
@connect(mapStateToProps)
export default class TabBarIconContainer extends PureComponent {
  static propTypes = {
    icon: PropTypes.any,
    tintColor: PropTypes.string,
    css: PropTypes.any,
    carts: PropTypes.object,
    cartIcon: PropTypes.any,
    wishlist: PropTypes.any,
    wishlistIcon: PropTypes.any,
  };

  _renderNumberWrap = (number = 0) => {
    return (
      <View style={styles.numberWrap}>
        <Text style={styles.number}>{number}</Text>
      </View>
    );
  };

  render() {
    const {
      icon,
      tintColor,
      css,
      carts,
      cartIcon,
      wishlist,
      wishlistIcon,
    } = this.props;

    return (



      <View style={{ justifyContent: "center" }}>
        {this.setState({ tintColor: this.props.tintColor = Color.primary })}
        {cartIcon && carts.total > 0 && <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: SIZE,
          height: SIZE,
          borderRadius: SIZE / 2,
          backgroundColor: Color.textColor
        }}>
          <Image
            ref={(comp) => (this._image = comp)}
            source={Images.IconCartWhite}
            style={[styles.icon, css]} />
          <View style={styles.numberWrapCart}>
            <Text style={styles.number}>{carts.total || 0}</Text>
          </View>

        </View>}
        <Image
          ref={(comp) => (this._image = comp)}
          source={icon}
          style={[styles.icon, { tintColor }, css]}
        />
        {wishlistIcon &&
          wishlist.total > 0 &&
          this._renderNumberWrap(wishlist.total || 0)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
    resizeMode: "contain",
  },
  numberWrap: {
    ...Styles.Common.ColumnCenter,
    position: "absolute",
    top: -10,
    right: -10,
    height: 18,
    minWidth: 18,
    backgroundColor: Color.primary,
    borderRadius: 9,
  },
  numberWrapCart: {
    ...Styles.Common.ColumnCenter,
    position: "absolute",
    top: 7,
    
    height: 18,
    minWidth: 18,
    backgroundColor: 'transparent',
    borderRadius: 9,
    alignItems:'center'
  },
  number: {
    color: "white",
    fontSize: 12,
    marginLeft: 3,
    marginRight: 3,
  },
});
