/**
 * created by Inspire UI @author(dang@inspireui.com)
 * @format
 */

import store from "@store/configureStore";
import {
  getUserInfo,
  checkCheckout,
  getPaymentSettings,
} from "@redux/operations";
import { cleanCart } from "@redux/actions";
import * as actions from "./actions";

/**
 * initial app
 */
export const initialApp = () => (dispatch) => {
  dispatch(actions.beginInitApp());

  const state = store.getState();
  const { carts, user } = state;
  const { accessToken, expiresAt } = user;

  if (accessToken) {
    dispatch(getUserInfo({ accessToken, expiresAt }));
  }
  if (carts.checkoutId) {
    if (carts.cartItems && carts.cartItems.length === 0) {
      dispatch(cleanCart());
    } else {
      dispatch(checkCheckout({ checkoutId: carts.checkoutId }));
    }
  }
  dispatch(getPaymentSettings());
};
