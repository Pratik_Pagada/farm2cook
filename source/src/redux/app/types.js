/**
 * created by Inspire UI @author(dang@inspireui.com)
 * @format
 */

const INITIAL_APP = "app/INITIAL";

const FINISH_INTRO = "app/FINISH_INTRO";

/**
 * notification
 */
const NOTIFICATION_ENABLE = "app/notification/ENABLE";
const NOTIFICATION_DISABLE = "app/notification/DISABLE";
const NOTIFICATION_TOGGLE = "app/notification/TOGGLE";

/**
 * currency
 */
const CURRENCY_CHANGE = "app/currency/CHANGE";

/**
 * language
 */
const LANGUAGE_CHANGE = "app/language/CHANGE";
const RTL_CHANGE = "app/rtl/CHANGE";

/**
 * netinfo
 */
const UPDATE_CONNECTION_STATUS = "app/network/UPDATE_STATUS";

/**
 * toast
 */
const ADD_TOAST = "app/toast/ADD";
const REMOVE_TOAST = "app/toast/REMOVE";

/**
 * sidemenu
 */
const SIDEMENU_OPEN = "app/sidemenu/OPEN";
const SIDEMENU_CLOSE = "app/sidemenu/CLOSE";
const SIDEMENU_TOGGLE = "app/sidemenu/TOGGLE";

export {
  INITIAL_APP,
  FINISH_INTRO,
  NOTIFICATION_ENABLE,
  NOTIFICATION_DISABLE,
  NOTIFICATION_TOGGLE,
  CURRENCY_CHANGE,
  LANGUAGE_CHANGE,
  RTL_CHANGE,
  UPDATE_CONNECTION_STATUS,
  SIDEMENU_OPEN,
  SIDEMENU_CLOSE,
  SIDEMENU_TOGGLE,
  ADD_TOAST,
  REMOVE_TOAST,
};
