/** @format */
import { Color, Images, Styles } from "@common";
import { TabBarIconContainer, CategoryContainer } from "@containers";

import React, { Component } from "react";
import { NavBarBack, NavBarLogo,NavBarTitle,NavBarEmpty } from "@components";
import { toast } from "@app/Omni";


export default class CategoryScreen extends Component {

  // static navigationOptions = ({ navigation }) => {
  //   const name = navigation.getParam("name");
  //   // const item = navigation.getParam("item");
  //   toast(name);
  //   return {
  //     headerLeft: NavBarBack({ navigation }),
  //     headerTitle: NavBarTitle({ navigation, title: name }),
  //     navigationOptions: {
  //       tabBarVisible: false,
  //     },
  //     headerStyle: Styles.Common.headerStyle,
  //     headerTitleStyle: Styles.Common.headerTitleStyle,
  //     };
  // };

  static navigationOptions = ({ navigation }) => ({
    
    headerTitle: NavBarLogo(),
    headerLeft: NavBarBack({ navigation }),
    headerRight: NavBarEmpty(),
    // tabBarIcon: ({ tintColor }) => (
    //   <TabBarIconContainer
    //     css={{ width: 18, height: 18 }}
    //     icon={Images.IconCategory}
    //     tintColor={tintColor}
    //   />
    // ),

    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.headerStyle,
    headerTitleStyle: Styles.Common.headerTitleStyle,
  });

  render() {
    return <CategoryContainer {...this.props} />;
  }
}
