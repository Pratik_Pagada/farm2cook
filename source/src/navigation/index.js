/** @format */

import React from "react";
import { Color, Images } from "@common";
import { TabBar } from "@components";
import { View, I18nManager, StyleSheet, Animated } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator,
  NavigationActions,
} from "react-navigation";
import { TabViewPagerPan } from "react-native-tab-view";
import { TabBarIconContainer } from "@containers";
import HomeScreen from "./HomeScreen";
import NewsDetailScreen from "./NewsDetailScreen";
import CategoriesScreen from "./CategoriesScreen";
import CategoryScreen from "./CategoryScreen";
import ProductDetailScreen from "./ProductDetailScreen";
import CartScreen from "./CartScreen";
import WishlistScreen from "./WishListScreen";
import SearchScreen from "./SearchScreen";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import CustomPageScreen from "./CustomPageScreen";
import ListAllScreen from "./ListAllScreen";
// import SettingScreen from "./SettingScreen";
import UserProfileScreen from "./UserProfileScreen";
import UserAddressScreen from "./UserAddressScreen";
import MyOrdersScreen from "./MyOrdersScreen";
import UserAddressFormScreen from "./UserAddressFormScreen";
// import AddCreditCardScreen from "./AddCreditCardScreen";
import NewLocation from "../components/NewLocation/NewLocation";
import TransitionConfig from "./TransitionConfig";

const CategoryStack = createStackNavigator(
  {
    CategoriesScreen: { screen: CategoriesScreen },
    CategoryScreen: { screen: CategoryScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const CategoryDetailStack = createStackNavigator(
  {
    CategoryScreen: { screen: CategoryScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

// const WishlistStack = createStackNavigator(
//   {
//     WishlistScreen: { screen: WishlistScreen },
//   },
//   {
//     navigationOptions: {
//       gestureDirection: I18nManager.isRTL ? "inverted" : "default",
//     },
//   }
// );

const SearchStack = createStackNavigator(
  {
    Search: { screen: SearchScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const HomeStack = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    ListAllScreen: { screen: ListAllScreen },
    NLocation: { screen: NewLocation }
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const CartScreenStack = createStackNavigator(
  {
    Cart: { screen: CartScreen },
    UserAddressScreen: { screen: UserAddressScreen },
    MyOrdersScreen: { screen: MyOrdersScreen },
    UserAddressFormScreen: { screen: UserAddressFormScreen },
    // AddCreditCardScreen: { screen: AddCreditCardScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const UserProfileStack = createStackNavigator(
  {
    UserProfile: { screen: UserProfileScreen },
    WishlistScreen: { screen: WishlistScreen },
    UserAddressScreen: { screen: UserAddressScreen },
    MyOrdersScreen: { screen: MyOrdersScreen },
    UserAddressFormScreen: { screen: UserAddressFormScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const MyOrdersStack = createStackNavigator(
  {
    MyOrders: { screen: MyOrdersScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Default: {
      screen: HomeStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabBarIconContainer icon={Images.IconHome} tintColor={tintColor} />
        ),
      },
    },
    CategoriesScreen: {
      screen: CategoryStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabBarIconContainer
            css={{ width: 18, height: 18 }}
            icon={Images.menuIcon}
            tintColor={tintColor}
          />
        ),
      },
    },
    CartScreen: {
      screen: CartScreenStack,
      
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabBarIconContainer
            cartIcon
            icon={Images.IconCart}
            tintColor={tintColor}
          />
        ),
        tabBarVisible: false,
      },
    },

    Search: {
      screen: SearchStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabBarIconContainer
            css={{ width: 18, height: 18 }}
            icon={Images.IconSearch}
            tintColor={tintColor}
          />
        ),
      },
    },    
    // MyOrdersScreen: {
    //   screen: MyOrdersStack,
    //   navigationOptions: {
    //     tabBarIcon: ({ tintColor }) => (
    //       <TabBarIconContainer
    //         orderIcon
    //         css={{ width: 18, height: 18 }}
    //         icon={Images.IconOrder}
    //         tintColor={tintColor}
    //       />
    //     ),
    //   },
    // },
    UserProfileScreen: {
      screen: UserProfileStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabBarIconContainer
            // wishlistIcon
            css={{ width: 18, height: 18 }}
            icon={Images.IconUser}
            tintColor={tintColor}
          />
        ),
      },
    },
    // SettingScreen: { screen: SettingScreen },

    CategoryDetail: { screen: CategoryDetailStack },
  },
  {
    // initialRouteName: "CartScreen",
    tabBarComponent: TabBar,
    tabBarPosition: "bottom",
    swipeEnabled: false,
    animationEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: Color.tabbarTint,
      inactiveTintColor: Color.tabbarColor,
    },
    lazy: true,
  }
);

TabNavigator.navigationOptions = () => {
  return {
    // fix header show when open drawer
    header: <View />,
    headerStyle: {
      backgroundColor: "transparent",
      height: 0,
      paddingTop: 0,
      borderBottomColor: "transparent",
      borderBottomWidth: 0,
    },
    gestureDirection: I18nManager.isRTL ? "inverted" : "default",
  };
};

const AppNavigator = createStackNavigator(
  {
    Tab: TabNavigator,
    Detail: ProductDetailScreen,
    News: NewsDetailScreen,
    Login: LoginScreen,
    Register: RegisterScreen,
    CustomPage: CustomPageScreen,
  },
  {
    mode: "modal",
    transitionConfig: () => TransitionConfig,
  }
);

export default AppNavigator;

/**
 * prevent duplicate screen
 */
const navigateOnce = (getStateForAction) => (action, state) => {
  const { type, routeName } = action;
  return state &&
    type === NavigationActions.NAVIGATE &&
    routeName === state.routes[state.routes.length - 1].routeName
    ? null
    : getStateForAction(action, state);
};

/**
 * Add AppNavigator to navigateOnce bug naivgate drawer category
 */
// AppNavigator.router.getStateForAction = navigateOnce(
//     AppNavigator.router.getStateForAction
// );
CategoryStack.router.getStateForAction = navigateOnce(
  CategoryStack.router.getStateForAction
);
CategoryDetailStack.router.getStateForAction = navigateOnce(
  CategoryDetailStack.router.getStateForAction
);
// WishlistStack.router.getStateForAction = navigateOnce(
//   WishlistStack.router.getStateForAction
// );
HomeStack.router.getStateForAction = navigateOnce(
  HomeStack.router.getStateForAction
);
SearchStack.router.getStateForAction = navigateOnce(
  SearchStack.router.getStateForAction
);
CartScreenStack.router.getStateForAction = navigateOnce(
  CartScreenStack.router.getStateForAction
);

/**
 * FIX RTL react-navigation tab do not show
 */
TabViewPagerPan.prototype.render = function render() {
  const { panX, offsetX, navigationState, layout, children } = this.props;
  const { width } = layout;
  const { routes } = navigationState;
  const maxTranslate = width * (routes.length - 1);
  let translateX;
  if (I18nManager.isRTL) {
    // <------- HACK ---------
    translateX = Animated.multiply(Animated.add(panX, offsetX), -1).interpolate(
      {
        inputRange: [0, maxTranslate],
        outputRange: [0, maxTranslate],
        extrapolate: "clamp",
      }
    );
    // ---------------------->
  } else {
    translateX = Animated.add(panX, offsetX).interpolate({
      inputRange: [-maxTranslate, 0],
      outputRange: [-maxTranslate, 0],
      extrapolate: "clamp",
    });
  }

  return (
    <Animated.View
      style={[
        styles.sheet,
        width
          ? {
              width: routes.length * width,
              transform: [{ translateX }],
            }
          : null,
      ]}
      {...this._panResponder.panHandlers}>
      {React.Children.map(children, (child, i) => (
        <View
          key={navigationState.routes[i].key}
          testID={navigationState.routes[i].testID}
          style={
            width
              ? { width }
              : i === navigationState.index
              ? StyleSheet.absoluteFill
              : null
          }>
          {i === navigationState.index || width ? child : null}
        </View>
      ))}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  sheet: {
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch",
  },
});
