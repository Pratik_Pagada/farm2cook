/** @format */

import React, { Component } from "react";
import { Color, Styles } from "@common";
import { MyOrdersContainer } from "@containers";
import { NavBarBack,NavBarMenu, NavBarLogo,NavBarTitle } from "@components";

export default class MyOrdersScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const name = navigation.getParam("name");
    return {
      headerLeft: NavBarBack({ navigation }),
      headerTitle: NavBarTitle({ navigation, title: name }),
      headerStyle: Styles.Common.headerStyle,
      headerTitleStyle: Styles.Common.headerTitleStyle,
    };
  };

  // static navigationOptions = ({ navigation }) => ({
  //   headerLeft: NavBarBack({ navigation }),
  //   headerTitle: NavBarLogo({ navigation }),
  //   // headerLeft: NavBarMenu({ navigation }),
  //   headerTintColor: Color.headerTintColor,
  //   headerStyle: Styles.Common.headerStyle,
  // });

  render() {
    return <MyOrdersContainer {...this.props} />;
  }
}
