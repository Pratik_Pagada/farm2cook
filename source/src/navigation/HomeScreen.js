/** @format */

import React, { Component } from "react";
import { HomeContainer } from "@containers";
import { View, Text, TouchableOpacity, Image ,AsyncStorage} from 'react-native';

const location = require("../images/icons/icon-pin.png");


export default class HomeScreen extends Component {

  constructor(props) {
    super(props)
    this.state = ({
      selectedLocation: 'Location'
    })
  }
  static navigationOptions = {
    header: null
  }

  gotoLocation = () => {
    //this.props.navigation.navigate('NLocation')
    this.props.navigation.navigate('NLocation', {
      onGoBack: () => this.refresh(),
    });
  }

  async refresh() {


    AsyncStorage.getItem('chooselocation')
      .then((value) => {

        if (value != null) {
          this.setState({
            selectedLocation: value,
          });
        }
      }
      );
  }

  render() {
    //return <HomeContainer {...this.props} />;
    // return <View><Text></Text></View>;
    return (
      <View style={{ height: '100%', width: '100%', marginTop: 25 }}>
        <View style={{ flex: 1, width: '100%', height: 50, position: 'absolute', alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity activeOpacity={0.9}>
            <View style={{ flexDirection: 'row' }}>
              {/* <Image style={{ height: 20, width: 15, justifyContent: 'center', marginRight: 10 }} source={location} /> */}
              <Text style={{ justifyContent: 'center',fontWeight:'bold' }}>{'FARM2COOK'}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 50, height: '100%', width: '100%', backgroundColor: 'orange' }}>
          <HomeContainer {...this.props} />
        </View>
      </View>
    );
  }
}
