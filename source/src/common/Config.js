/** @format */

import Images from "./Images";
import Constants from "./Constants";
import Icons from "./Icons";

export default {
  /**
   * Step 1: change to your website shopify URL, apiKey, graphqlUrl, and storeAccessToken
   * https://help.shopify.com/en/api/custom-storefronts/storefront-api/getting-started
   */
  Shopify: {
    url: "farm2cook.myshopify.com",
    apiKey: "acbf2ae83fbe6d582198546e9089c420",
    graphqlUrl: "https://farm2cook.myshopify.com/api/graphql",
    storeAccessToken: "b206e7b95003af696cff80e43376a79a",
  },

  Wordpress: {
    url: "https://www.farm2cook.com/",
  },

  /**
     Step 2: Setting Product Images
     - ProductSize: Explode the guide from: update the product display size: https://mstore.gitbooks.io/mstore-manual/content/chapter5.html
     The default config for ProductSize is disable due to some problem config for most of users.
     If you have success config it from the Wordpress site, please enable to speed up the app performance
     - HorizonLayout: Change the HomePage horizontal layout - https://mstore.gitbooks.io/mstore-manual/content/chapter6.html
     */
  ProductSize: {
    enable: false,
    CatalogImages: { width: 300, height: 360 },
    SingleProductImage: { width: 600, height: 720 },
    ProductThumbnails: { width: 180, height: 216 },
  },

  /**
   * Step 3: Setting Home screen, category screen
   * Follow this article to use Graphql https://help.shopify.com/en/api/custom-storefronts/storefront-api/graphql
   * Video tutorial https://www.dropbox.com/s/m4kmp0quh8qplz3/Screen%20Recording%202018-11-05%20at%202.22.21%20PM.mov?dl=0
   */
  SpaceLayout: 2, // 15 setting align right 
  HorizonLayout: [
    {
      categoryId: "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzY1MTkzODM2NTg2",
      paging: true,
      layout: Constants.Layout.miniBanner,
    },
    {
      name: "Featured Products",
      categoryId: "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzY1NTYwMTUwMDU4",
      layout: Constants.Layout.twoColumn,
      // layout: Constants.Layout.card,
    },
    {
      name: "Chicken",
      categoryId: "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzY1MTkzODAzODE4",
      layout: Constants.Layout.twoColumn,
    },
    {
      name: "Lamb & Goat",
      categoryId: "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzY1MTkzODM2NTg2",
      layout: Constants.Layout.twoColumn,
      // layout: Constants.Layout.twoColumnHigh,
    },
    {
      name: "Seafood",
      categoryId: "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzY1NTU5OTUzNDUw",
      layout: Constants.Layout.twoColumn,
      // layout: Constants.Layout.card,
    },
    {
      name: "Farm2Cafe",
      categoryId: "Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzY1NTYwMDUxNzU0",
      layout: Constants.Layout.twoColumn,
      // layout: Constants.Layout.card,
    },
  ],

  CategoryLayout: {
    ListMode: "ListMode",
    GridMode: "GridMode",
    CardMode: "CardMode",
  },

  /**
   * Now we only support Webview payment, so disable this feature
   * In the furture we will try support this feature
   */
  Payments: [
    // {
    //   name: "Cash on Delivery",
    //   imageUrl: require("@images/payment_logo/cash_on_delivery.png"),
    //   enabled: true,
    // },
    // {
    //   type: "card",
    //   name: "Credit Card",
    //   imageUrl: require("@images/payment_logo/credit_card.png"),
    //   enabled: true,
    // },
  ],

  /**
     Step 4: Advance config:
     - showShipping: option to show the list of shipping method
     - showStatusBar: option to show the status bar, it always show iPhoneX
     - LogoImage: The header logo
     - CustomPages: Update the custom page which can be shown from the left side bar (Components/Drawer/index.js)
     - intro: The on boarding intro slider for your app
     - menu: config for left menu side items (isMultiChild: This is new feature from 3.4.5 that show the sub products categories)
     * */
  shipping: {
    visible: true,
    time: {
      free_shipping: "4 - 7 Days",
      flat_rate: "1 - 4 Days",
      local_pickup: "1 - 4 Days",
    },
  },
  showStatusBar: true,
  showShipping:true,
  LogoImage: require("@images/logo.png"),
  CustomPages: { contact_id: 10941 },

  /**
   * Config Menu Side Drawer
   * @param goToScreen 3 Params (routeName, params, isReset = false)
   * BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
   */
  menu: {
    // has child categories
    isMultiChild: false,
    // Unlogged
    listMenuUnlogged: [
      {
        text: "Login",
        routeName: "Login",
        params: {
          isLogout: false,
        },
        icon: Icons.MaterialCommunityIcons.SignIn,
      },
    ],
    // user logged in
    listMenuLogged: [
      {
        text: "Logout",
        routeName: "Login",
        params: {
          isLogout: true,
        },
        icon: Icons.MaterialCommunityIcons.SignOut,
      },
    ],
    // Default List
    listMenu: [
      {
        text: "Shop",
        routeName: "Default",
        icon: Icons.MaterialCommunityIcons.Home,
      },
       {
        text: "About Us",
        routeName: "CustomPage",
        params: {
         //  url: "http://inspireui.com",
         url:"https://www.farm2cook.com/pages/about-us",  name:"About Us"
        },
      //  icon: Icons.MaterialCommunityIcons.Email,0
      },
      {
        text: "Contact Us",
        routeName: "CustomPage",
        params: {
        
         url:"https://www.farm2cook.com/pages/contact-us",  name:"Contact Us"
         

        },
      //  icon: Icons.MaterialCommunityIcons.Email,0
      },
      {
        text: "Terms Of Service",
        routeName: "CustomPage",
        params: {
         //  url: "http://inspireui.com",
         url:"https://www.farm2cook.com/pages/terms-of-service",  name:"Terms Of Service"
        },
      //  icon: Icons.MaterialCommunityIcons.Email,0
      },
      {
        text: "Refund Policy",
        routeName: "CustomPage",
        params: {
        
         url:"https://www.farm2cook.com/pages/refund-policy",  name:"Refund Policy"
        },
      //  icon: Icons.MaterialCommunityIcons.Email,0
      },
      {
        text: "Privacy Policy",
        routeName: "CustomPage",
        params: {
         //  url: "http://inspireui.com",
         url:"https://www.farm2cook.com/pages/privacy-policy",  name:"Privacy Policy"
        },
      //  icon: Icons.MaterialCommunityIcons.Email,0
      },
    
    ],
  },
};
