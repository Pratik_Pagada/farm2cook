/** @format */

import React from "react";
import { Image, YellowBox } from "react-native";
import { AppLoading, Asset, Font } from "@expo";
import Reactotron from "reactotron-react-native";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/es/integration/react";
import { Provider } from "react-redux";
import store from "@store/configureStore";
import RootRouter from "./src/Router";
import "./ReactotronConfig";

function cacheImages(images) {
  return images.map((image) => {
    if (typeof image === "string") {
      return Image.prefetch(image);
    }
    return Asset.fromModule(image).downloadAsync();
  });
}

function cacheFonts(fonts) {
  return fonts.map((font) => Font.loadAsync(font));
}

console.ignoredYellowBox = [
  "Warning: View.propTypes",
  "Warning: BackAndroid",
  "Require cycle:",
];
// fix warning Require cycle expo 31
YellowBox.ignoreWarnings(["Require cycle:"]);

export default class App extends React.Component {
  state = { appIsReady: false };

  componentWillMount() {
    if (__DEV__) {
      Reactotron.connect();
      Reactotron.clear();
    }
  }

  loadAssets = async () => {
    const fontAssets = cacheFonts([
      { Dosis: require("@assets/fonts/Dosis-Regular.ttf") },
      { "Dosis-Bold": require("@assets/fonts/Dosis-Bold.ttf") },
      { Baloo: require("@assets/fonts/Baloo-Regular.ttf") },
      // { Entypo: require("@expo/vector-icons/fonts/Entypo.ttf") },
      // {
      //   "Material Icons": require("@expo/vector-icons/fonts/MaterialIcons.ttf"),
      // },
      // {
      //   MaterialCommunityIcons: require("@expo/vector-icons/fonts/MaterialCommunityIcons.ttf"),
      // },
      // {
      //   "Material Design Icons": require("@expo/vector-icons/fonts/MaterialCommunityIcons.ttf"),
      // },
      // { FontAwesome: require("@expo/vector-icons/fonts/FontAwesome.ttf") },
      // {
      //   "simple-line-icons": require("@expo/vector-icons/fonts/SimpleLineIcons.ttf"),
      // },
      { Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf") },
    ]);

    const imageAssets = cacheImages([
      require("@images/checkout/header_cart.png"),
      // Images.icons.iconColumn,
      // Images.icons.iconLeft,
      // Images.icons.iconRight,
      // Images.icons.iconThree,
      // Images.icons.iconAdvance,
      // Images.icons.iconHorizal,
      // Images.icons.back,
      // Images.icons.home,
      // Images.IconSwitch,
      // Images.IconFilter,
      // Images.IconList,
      // Images.IconGrid,
      // Images.IconCard,
      // Images.IconSearch,
      // Images.IconHome,
      // Images.IconCategory,
      // Images.IconHeart,
      // Images.IconOrder,
      // Images.IconCart,
    ]);

    await Promise.all([...fontAssets, ...imageAssets]);
  };

  render() {
    const persistor = persistStore(store);
   

    if (!this.state.appIsReady) {
     
      return (
        <AppLoading
          startAsync={this.loadAssets}
          onFinish={() => this.setState({ appIsReady: true })}
        />
        // <Image style={{height:'100%',width:'100%'}}
        // source={require("../source/assets/splash.png")}/>
      );
    }

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <RootRouter />
        </PersistGate>
      </Provider>
    );
  }
}
